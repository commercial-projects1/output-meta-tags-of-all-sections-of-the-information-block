<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
if($USER->IsAdmin()){
   CModule::IncLudeModule('iblock');
   $iblockId = 95;
   $iblockType = "1c_catalog";
   $arFilter = ['IBLOCK_ID' => $iblockId];   
   $bShowCount = false; //количество элементов в разделе
   $arSelect = ["ID","NAME","IBLOCK_SECTION_ID"]; //список полей

   $arPrint = [
      "SECTION_PAGE_TITLE"=>[
         "TITLE"=>"h1",
         "MIN_LENGTH" => 0,
         "MAX_LENGTH" => 60
      ],   
      "SECTION_META_TITLE"=>[
         "TITLE"=>"meta title",
         "MIN_LENGTH" => 1,
         "MAX_LENGTH" => 80,
      ],
      "SECTION_META_KEYWORDS"=> [
         "TITLE"=>"keywords",
         "MIN_LENGTH" => 1,
         "MAX_LENGTH" => 10000
      ],
      "SECTION_META_DESCRIPTION"=>[
         "TITLE"=>"description",
         "MIN_LENGTH" => 100,
         "MAX_LENGTH" => 300
      ]
      
   ];   

   $rsSect = CIBlockSection::GetList(array('id' => 'asc'),$arFilter,$bShowCount,$arSelect);?>
   
   <table class="table" cellpadding="0" cellspacing="0">
      <tr>
         <td>Prop</td>
         <td>min</td>
         <td>max</td>
      </tr>
      <?foreach($arPrint as $item):?>
         <tr>
            <td><?=$item["TITLE"]?></td>
            <td><?=$item["MIN_LENGTH"]?></td>
            <td><?=$item["MAX_LENGTH"]?></td>
         </tr>
      <?endforeach;?>
   </table>

   <hr>
   <br>
   
   <?while ($arSect = $rsSect->GetNext()):?>
      
      
      <table class="table" cellpadding="0" cellspacing="0">
         <tr>
            <td colspan="3"><a target="_blank"   href="/bitrix/admin/iblock_section_edit.php?IBLOCK_ID=<?=$iblockId?>&type=<?=$iblockType?>&lang=ru&ID=<?=$arSect["ID"]?>&find_section_section=<?=$arSect["IBLOCK_SECTION_ID"]?>">[<?=$arSect["ID"]?>] <?=$arSect["NAME"]?></a></td>
         </tr>   
         <?$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($iblockId,$arSect["ID"]); 
         $arSeoProps = $ipropValues->getValues();
         foreach($arPrint as $key => $tag):         
            if(isset($arSeoProps[$key])):
               $len = strlen($arSeoProps[$key]);?>
               <tr>
                  <td><?=$arPrint[$key]["TITLE"]?></td>
                  <td>
                     <span <?=(($len<$arPrint[$key]["MIN_LENGTH"])||($len>$arPrint[$key]["MAX_LENGTH"]))?"class='danger'":""?>><?=$len?></span></td>
                  <td><?=$arSeoProps[$key]?></td>
               </tr>                      
            <?endif;
         endforeach;?>     
      </table> 
     
   <?endwhile;?>
   <style>
      .table{
         margin-bottom: 24px;
          border: 3px solid #d1d1d1;
          width: 800px;
          margin-left: calc(50% - 400px);
      }
      .table td{
         padding: 7px 17px;
          font-size: 14px;
          border-top: 1px solid #c7c7c7;
      }
      .table .danger{
         color: red;
      }
   </style>

   
<?}
else{
   LocalRedirect("/");
}
?>